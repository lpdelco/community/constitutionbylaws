# Seasoning Requirement | Bylaws LP Delco

## ARTICLE II.MEMBERSHIP

### Section 1 - Establishing Membership

Any person shall be considered a member of the Delaware County Libertarian Committee who meets the following criteria:

1. Adheres to libertarian principles, and who generally supports the purposes and principles of the National Libertarian Party;

2. Attends three consecutive past meetings in the last three (3) years. 

3. Pays dues when applicable.
