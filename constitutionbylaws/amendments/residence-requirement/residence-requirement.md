## Residence Requirement | Bylaws LP Delco

## ARTICLE II.MEMBERSHIP

### Section 1 - Establishing Membership

4. have vested interest in Delaware county in the form of property, business, residence, being a full-time student or being the legal guardian of a student within the county. Exceptions may be made with a two-thirds (2/3) vote of the members in attendance.
