% Committee Constitution
% Libertarian Party of Delaware County
% March 22, 2005

ARTICLE I.NAME
==============

The name of this committee shall be the Delaware County Committee of the
Libertarian Party of Pennsylvania, hereinafter referred to as the
Committee.

ARTICLE II.PURPOSE
==================

The purpose of the Committee is to proclaim and to implement the
Statement of Principles of the National Libertarian Party by engaging in
political and educational activities in Delaware County in the
Commonwealth of Pennsylvania.

ARTICLE III.MEMBERSHIP
======================

An individual endorsing the purposes and principles of the National
Libertarian Party may become and remain a member of the Committee
subject to the provisions of the Constitution, Bylaws, and Rules.

ARTICLE IV.ORGANIZATION
=======================

Section 1 - Officers
--------------------

The officers of the Committee shall be a Chair, a Vice Chair, a
Secretary, and a Treasurer. All of these officers shall be elected at an
annual Winter meeting of the Committee by attending members and shall
take office immediately upon the close of such meeting.

Section 2 - Board of Directors 
------------------------------

The Board of Directors shall be responsible for the control and
management of all the affairs, properties and funds of the Committee
consistent with this Constitution, its Bylaws and its Rules. The Board
of Directors of the Committee shall be composed of the following:

-   The four elected officers of the Committee.
-   The immediate past chair
-   One member, chosen, by the committee, of each of the standing
    committees.

Section 3 - Judicial Committee
------------------------------

The Judicial Committee shall be composed of three members appointed by
the Board of Directors. The term of a member of the Judicial Committee
shall run through the period of the next Winter meeting and until a
successor is appointed. A member of the Judicial Committee may not serve
as an officer of the Libertarian Party of Pennsylvania or as an officer
of any County Committee of the Libertarian Party of Pennsylvania.

The Judicial Committee shall be the final body of appeal in all matters
regarding interpretations of the Constitution, Bylaws, or Rules of the
Committee, subject to the provisions that a decision of the Judicial
Committee can be overturned by a three quarters vote of the Winter
meeting of the Committee.

ARTICLE V.ANNUAL WINTER MEETING
===============================

The Committee shall hold an annual Winter meeting to conduct such
business as may properly come before it at a time and place to be set
according to the Bylaws and in compliance with the Constitution, Bylaws,
and Rules. This meeting shall occur prior to the Spring meeting of the
Libertarian Party of Pennsylvania.

ARTICLE VI.BYLAWS
=================

The Bylaws are hereby affixed to and subordinate to the Constitution.

ARTICLE VII.AMENDMENTS
======================

The Constitution may be amended by a two thirds vote of all members in
attendance at a Winter meeting of the Committee.
