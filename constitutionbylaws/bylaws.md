% Committee Bylaws
% Libertarian Party of Delaware County
% March 22, 2005

ARTICLE I.PURPOSE AND SCOPE
===========================

Section 1 - Purpose
-------------------

The purpose of the Committee is to conduct the following activities
consistent with the Statement of Principles:

a)  Disseminate Libertarian political philosophy by entering into
    political information activities;
b)  Nominating and supporting candidates for countywide and local
    political office;
c)  Supporting statewide Libertarian Party Candidates;
d)  Supporting national Libertarian Party Candidates.

Section 2 - Scope
-----------------

The Committee shall conduct its activities primarily within Delaware
County in the Commonwealth of Pennsylvania. The Committee may coordinate
its efforts with County Committees in neighboring Counties. It may also,
with the approval of the state party, organize libertarian activities in
neighboring Counties without County Libertarian Organizations.

ARTICLE II.MEMBERSHIP
=====================

Section 1 - Establishing Membership
-----------------------------------

Any person who consistently attends and supports Committee functions,
who adheres to libertarian principles, and who generally supports the
purposes and principles of the National Libertarian Party shall be
considered a member of the Delaware County Libertarian Committee.

Section 2 - Eligibility for Committee officers
----------------------------------------------

Any person who is nominated to be an officer of the Committee must be a
member in good standing of the Libertarian Party of Pennsylvania, and a
registered Libertarian with the County of his/her residence.

Section 3 - Dues
----------------

The Committee intends to operate on voluntary contributions. However,
the Board of Directors may from time to time determine the amount of
dues necessary for membership in the Committee, and may determine one or
more classes of membership with different amounts of dues for each.

Section 4 - Termination of Membership
-------------------------------------

The Board of Directors shall have the power to suspend a member for
cause by a two-thirds vote of the Board. A member may be suspended \"for
cause\" for misrepresenting the principles of the Committee, endorsing
or campaigning for, in the name of the Committee, a candidate for public
office in opposition to one nominated by the Committee or by the State
or National Libertarian Party, or for running for office purporting to
have been nominated or endorsed by the Committee without having received
such nomination or endorsement, or for other reasonable cause.

Notice of suspension is subject to written appeal within fifteen days of
notification. Failure to appeal shall terminate membership.

Section 5 - Membership Appeal
-----------------------------

Upon appeal by the member, the Judicial Committee shall hold a hearing
concerning the suspension. Following the hearing, the Judicial Committee
shall rule either to terminate the membership of the member or to
continue the member in good standing. Should the Judicial Committee fail
to rule, the member shall continue as a member of the Committee.

ARTICLE III.OFFICERS
====================

Section 1 - Chair
-----------------

The Chair shall preside at all public meetings and at all meetings of
the Board of Directors. She or he shall be the chief executive officer
of the committee.

Section 2 - Vice Chair
----------------------

The Vice Chair shall act as assistant to the Chair.

Section 3 - Secretary
---------------------

The Secretary shall take and keep minutes of all party meetings and all
meetings of the Board of Directors and shall maintain current list of
all members of the Committee.

Section 4 - Treasurer
---------------------

The treasurer shall receive, disburse and account for the funds of the
Committee under supervision of the Chair and the Board of Directors. The
Treasurer shall compile a quarterly report, which shall consist of a
balance sheet and the profit and loss schedule shall be available to
members upon request. The treasurer shall file all required reports on
behalf of the Committee with government agencies.

Section 5 - Suspension
----------------------

An officer may be suspended from office by a two-thirds vote of the
Board of Directors. The office of a suspended officer shall be declared
vacant unless the suspended officer appeals suspension to the Judicial
Committee within ten days of notification of suspension.

Section 6 - Appeal
------------------

Upon written appeal by the suspended officer, the Judicial Committee
shall set the date of a hearing. Following the hearing, the Judicial
Committee shall rule within three days to either uphold the suspension
(thereby vacating the office) or restore the officer to full authority.
A failure to rule shall be deemed as restoring the officer to full
authority.

Section 7 - Vacancies
---------------------

The Board of Directors shall appoint new officers if vacancies or
suspensions occur, such officers to complete the term of office vacated.

ARTICLE IV.BOARD OF DIRECTORS
=============================

Section 1 - Meeting Notification
--------------------------------

The Board of Directors shall meet at such time and place as may be
determined by a call of the Chair or by the written request of one third
or more members of the B.O.D.

A notice of the time and place of all meetings shall be provided to each
member of the Board of Directors not less than seven days prior to said
meeting.

Section 2 - Public Meetings
---------------------------

The Board shall hold at least four public meetings each year, open to
all registered libertarian party voters.

ARTICLE V.THE JUDICIAL COMMITTEE
================================

Section 1 - Organization
------------------------

The Judicial Committee shall elect a chair who shall receive all appeals
and petitions and schedule hearings so as to obtain a quorum of the
Judicial Committee. When a hearing is requested the Chair shall be
allowed three days to set the date of the hearing.

Section 2 - Hearing Notification
--------------------------------

The Judicial Committee must provide at least ten days notice to each of
the interested parties to a hearing unless an earlier date is agreed to
by the Judicial Committee and the participants.

Section 3 - Limit
-----------------

Hearing must be held within thirty days from the time the request is
received by the Judicial Committee.

Section 4 - Representation
--------------------------

Each party to a hearing shall have the right to represent his or her
interest in the manner of his or her choosing.

Section 5 - Rulings
-------------------

The Judicial Committee must provide a ruling within three days of the
conclusion of a hearing.

ARTICLE VI.COMMITTEES
=====================

Section 1 - Standing Committees
-------------------------------

The standing committees of the Committee shall be the - to be determined
-. The duties, composition and reporting requirements shall be
determined from time to time by the Board.

Section 2 - Working Committees
------------------------------

There shall be such working Committee appointed by the Chair as the
Board of Directors deems appropriate. Working committees shall exist at
the discretion of the Board of Directors.

ARTICLE VII.NOMINATIONS OF CANDIDATES FOR OFFICE
================================================

Section 1 - Nominations
-----------------------

Candidates for County and local office shall be nominated by the
Committee in consultation with the appropriate Municipal Committees. The
Board of Directors shall be responsible for approving all nominations.
The Board of Directors shall have the power to nominate candidates for
Special Elections and nominate substitute candidates as provided in the
State Election Code.

ARTICLE VIII.PARLIAMENTARY AUTHORITY
====================================

Roberts Rules of Order as newly revised shall be the parliamentary
authority for all matters of procedure not specifically covered by these
Bylaws.
